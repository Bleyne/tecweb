<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});

Route::get('/fabricante', 'FabricanteController@create')->name('fabricante.create');
Route::post('/fabricante', 'FabricanteController@store')->name('fabricante.store');

Route::get('/pedido', 'FabricanteController@pedido')->name('fabricante.pedido');
Route::post('/pedido', 'FabricanteController@pedidoStore')->name('fabricante.pedidoStore');

Route::get('/fornecedor', 'FornecedorController@index')->name('fornecedor.index');
Route::post('/fornecedor', 'FornecedorController@index')->name('fornecedor.search');
Route::post('/fornecedor/create', 'FornecedorController@store')->name('fornecedor.store');
Route::get('/fornecedor/create', 'FornecedorController@create')->name('fornecedor.create');

Route::get('/produto', 'ProdutoController@index')->name('produto.index');
Route::post('/produto', 'ProdutoController@index')->name('produto.search');
Route::get('/produto/create', 'ProdutoController@create')->name('produto.create');
Route::post('/produto/create', 'ProdutoController@store')->name('produto.store');
Route::delete('/produto/remove/{id}', 'ProdutoController@destroy')->name('produto.destroy');