@extends('layouts.app')
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Cadastrar Fornecedores</h3>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(array('route' => 'fornecedor.store','method'=>'POST')) !!}
            <div class="box-body">
                <div class="form-group">
                    <div class="form-group{{ $errors->has('razao_social') ? ' has-error' : '' }}">
                        <label for="razao_social">Razão Social</label>
                        {!! Form::text('razao_social', null, array('placeholder' => 'Razão Social','class' => 'form-control')) !!}
                        {!! $errors->first('razao_social','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group{{ $errors->has('marca') ? ' has-error' : '' }}">
                        <label for="razao_social">Marca</label>
                        {!! Form::text('marca', null, array('placeholder' => 'Marca','class' => 'form-control')) !!}
                        {!! $errors->first('marca','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="sel1">Fabricante</label>
                    {!! Form::select('fabricante_id', $fabricantes, null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group{{ $errors->has('endereco') ? ' has-error' : '' }}">
                    <label for="endereco">Endereço</label>
                    {!! Form::text('endereco', null, array('placeholder' => 'Endereço','class' => 'form-control')) !!}
                    {!! $errors->first('endereco','<span class="help-block">:message</span>') !!}
                </div>
                <div class="form-group{{ $errors->has('telefone') ? ' has-error' : '' }}">
                    <label for="endereco">Telefone</label>
                    {!! Form::text('telefone', null, array('placeholder' => 'Telefone','class' => 'form-control')) !!}
                    {!! $errors->first('telefone','<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
        {!! Form::close() !!}
    </div>

@endsection