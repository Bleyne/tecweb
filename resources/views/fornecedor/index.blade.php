@extends('layouts.app')
@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Buscar Fornecedor</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(array('route' => 'fornecedor.search','method'=>'POST')) !!}
            <div class="box-body">
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('razao_social', $razao_social, array('placeholder' => 'Buscar fornecedor pelo nome','class' => 'form-control input-lg')) !!}
                        <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>

                    </span>
                    </div>
                </div>
        {!! Form::close() !!}
                <div class="form-group">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Fornecedor</th>
                            <th>Telefone</th>
                            <th>Contrato</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($fornecedores as $fornecedore)
                        <tr>
                            <td>{{$fornecedore->id}}</td>
                            <td>{{$fornecedore->razao_social}}</td>
                            <td>{{$fornecedore->telefone}}</td>
                            <td>Ativo</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </form>
    </div>
@endsection