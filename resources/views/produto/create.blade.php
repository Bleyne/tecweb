@extends('layouts.app')
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Cadastrar Produto</h3>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(array('route' => 'produto.store','method'=>'POST')) !!}
            <div class="box-body">
                <div class="form-group">
                    <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                        <label for="nome">Nome do Produto</label>
                        {!! Form::text('nome', null, array('placeholder' => 'Nome do Produto','class' => 'form-control')) !!}
                        {!! $errors->first('nome','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group{{ $errors->has('codigo') ? ' has-error' : '' }}">
                        <label for="codigo">Código do Produto</label>
                        {!! Form::text('codigo', null, array('placeholder' => 'Código do Produto','class' => 'form-control')) !!}
                        {!! $errors->first('codigo','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group{{ $errors->has('valor_compra') ? ' has-error' : '' }}">
                        <label for="razao_social">Valor de Compra</label>
                        {!! Form::text('valor_compra', null, array('placeholder' => 'Valor de Compra','class' => 'form-control')) !!}
                        {!! $errors->first('valor_compra','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group{{ $errors->has('valor_venda') ? ' has-error' : '' }}">
                        <label for="razao_social">Valor de Venda</label>
                        {!! Form::text('valor_venda', null, array('placeholder' => 'Valor de Venda','class' => 'form-control')) !!}
                        {!! $errors->first('valor_venda','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="sel1">Fabricante</label>
                    {!! Form::select('fabricante_id', $fabricantes, null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    <label for="sel1">Fornecedor</label>
                    {!! Form::select('fornecedors_id', $fornecedores, null, array('class' => 'form-control')) !!}
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
        {!! Form::close() !!}
    </div>

@endsection