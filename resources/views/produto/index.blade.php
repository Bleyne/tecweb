@extends('layouts.app')
@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Buscar Produtos</h3>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(array('route' => 'produto.search','method'=>'POST')) !!}
            <div class="box-body">
                <div class="form-group">
                    <div class="input-group">
                        {!! Form::text('nome', $nome, array('placeholder' => 'Buscar produto pelo nome','class' => 'form-control input-lg')) !!}
                        <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>

                    </span>
                    </div>
                </div>
        {!! Form::close() !!}
                <div class="form-group">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Produto</th>
                            <th>Codigo</th>
                            <th>Valor Compra</th>
                            <th>Valor Venda</th>
                            <th>Fabricante</th>
                            <th>Fornecedor</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($produtos as $produto)
                        <tr>
                            <td>{{$produto->nome}}</td>
                            <td>{{$produto->codigo}}</td>
                            <td>{{$produto->valor_compra}}</td>
                            <td>{{$produto->valor_venda}}</td>
                            <td>{{$produto->fabricante->razao_social}}</td>
                            <td>{{$produto->fornecedor->razao_social}}</td>
                            <td>
                                {{Form::open([ 'method'  => 'delete', 'route' => [ 'produto.destroy', $produto->id ] ])}}
                                {{ Form::submit('Remover', ['class' => 'btn btn-danger']) }}
                                {{ Form::close() }}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
        </form>
    </div>
@endsection