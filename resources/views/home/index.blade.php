@extends('layouts.app')
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h4>Através do Sistema 1.0, é possível:</h4>
            <li>Cadastrar novos produtos</li>
            <li>Buscar produto pelo nome ou pelo seu código</li>
            <li>Cadastrar novos fabricantes</li>
            <li>Gerar uma ordem de pedido para o fabricante</li>
            <li>Cadastrar fornecedores</li>
            <li>Listar fornecedores</li>

        </div>
        <!-- /.box-header -->
        <!-- form start -->
    </div>
@endsection