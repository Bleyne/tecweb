@extends('layouts.app')
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Cadastrar Fabricantes</h3>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <!-- form start -->
        {!! Form::open(array('route' => 'fabricante.store','method'=>'POST')) !!}
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <div class="form-group{{ $errors->has('razao_social') ? ' has-error' : '' }}">
                                <label for="razao_social">Razão Social</label>
                                {!! Form::text('razao_social', null, array('placeholder' => 'Razão Social','class' => 'form-control')) !!}
                                {!! $errors->first('razao_social','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group{{ $errors->has('codigo_fabricante') ? ' has-error' : '' }}">
                            <label for="codigo_fabricante">Código do Fabricante</label>
                            {!! Form::text('codigo_fabricante', null, array('placeholder' => 'Código do Fabricante','class' => 'form-control')) !!}
                            {!! $errors->first('codigo_fabricante','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('endereco') ? ' has-error' : '' }}">
                    <label for="endereco">Endereço</label>
                    {!! Form::text('endereco', null, array('placeholder' => 'Endereço','class' => 'form-control')) !!}
                    {!! $errors->first('endereco','<span class="help-block">:message</span>') !!}
                </div>
                <div class="form-group{{ $errors->has('telefone') ? ' has-error' : '' }}">
                    <label for="endereco">Telefone</label>
                    {!! Form::text('telefone', null, array('placeholder' => 'Telefone','class' => 'form-control')) !!}
                    {!! $errors->first('telefone','<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
        {!! Form::close() !!}
    </div>
@endsection