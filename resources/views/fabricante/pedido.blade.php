@extends('layouts.app')
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Fazer Pedido</h3>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(array('route' => 'fabricante.pedidoStore','method'=>'POST')) !!}
            <div class="box-body">
                <div class="form-group">
                    <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                        <label for="razao_social">Nome do Produto</label>
                        {!! Form::text('nome', null, array('placeholder' => 'Nome do Produto','class' => 'form-control')) !!}
                        {!! $errors->first('nome','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="sel1">Fabricante</label>
                    {!! Form::select('fabricante_id', $fabricantes, null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    <div class="form-group{{ $errors->has('qtde') ? ' has-error' : '' }}">
                        <label for="razao_social">Quantidade</label>
                        {!! Form::number('qtde', null, array('placeholder' => 'Quantidade','class' => 'form-control')) !!}
                        {!! $errors->first('qtde','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Gerar Pedido</button>
            </div>
        {!! Form::close() !!}
    </div>
@endsection