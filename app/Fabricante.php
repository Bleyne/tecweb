<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fabricante extends Model
{
    protected $fillable = [
        'razao_social', 'codigo_fabricante','endereco', 'telefone'
    ];
}
