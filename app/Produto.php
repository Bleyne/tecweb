<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Fabricante;
use App\Fornecedor;

class Produto extends Model
{
    protected $fillable = [
        'nome', 'codigo','valor_compra', 'valor_venda', 'fabricante_id', 'fornecedors_id'
    ];

    public function fabricante()
    {
        return $this->hasOne('App\Fabricante', 'id','fabricante_id');
    }

    public function fornecedor()
    {
        return $this->hasOne('App\Fornecedor', 'id','fornecedors_id');
    }
}
