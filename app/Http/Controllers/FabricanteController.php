<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fabricante;
use App\Pedido;

class FabricanteController extends Controller
{
    public function create()
    {
        return view('fabricante.create');
    }

    public function pedido()
    {
        $fabricantes = Fabricante::pluck( 'razao_social', 'id');
        return view('fabricante.pedido', compact('fabricantes',$fabricantes));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'razao_social' => 'required|max:255',
            'codigo_fabricante' => 'required|max:50',
            'endereco' => 'required|max:255',
            'telefone' => 'required|max:20',
        ]);

        Fabricante::create($request->all());
        return redirect()->route('fabricante.create')
            ->with('success','Cliente registrado com sucesso!');
    }

    public function pedidoStore(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|max:255',
            'fabricante_id' => 'required|exists:fabricantes,id',
            'qtde' => 'required|integer'
        ]);

        Pedido::create($request->all());
        return redirect()->route('fabricante.pedido')
            ->with('success','Pedido registrado com sucesso!');
    }

}
