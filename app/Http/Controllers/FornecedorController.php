<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fabricante;
use App\Fornecedor;

class FornecedorController extends Controller
{
    public function index(Request $request)
    {
        $razao_social = '';
        if ($request->has('razao_social')) {
            $razao_social = $request->razao_social;
            $fornecedores = Fornecedor::where('razao_social', 'like', "%{$request->razao_social}%")->get();
        } else {
            $fornecedores = Fornecedor::all();
        }
        return view('fornecedor.index', compact('fornecedores', 'razao_social' ));
    }

    public function create()
    {
        $fabricantes = Fabricante::pluck( 'razao_social', 'id');
        return view('fornecedor.create', compact('fabricantes',$fabricantes));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'razao_social' => 'required|max:255',
            'marca' => 'required|max:255',
            'fabricante_id' => 'required|exists:fabricantes,id',
            'endereco' => 'required|max:255',
            'telefone' => 'required|max:20',
        ]);

        Fornecedor::create($request->all());
        return redirect()->route('fornecedor.create')
            ->with('success','Fornecedor registrado com sucesso!');
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'razao_social' => 'required|max:255'
        ]);

        //Fornecedor::create($request->all());
        return redirect()->route('fornecedor.create')
            ->with('success','Fornecedor registrado com sucesso!');
    }
}
