<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fornecedor;
use App\Produto;
use App\Fabricante;

class ProdutoController extends Controller
{
    public function index(Request $request)
    {
        $nome = '';
        if ($request->has('nome')) {
            $nome = $request->nome;
            $produtos = Produto::where('nome', 'like', "%{$request->nome}%")->get();
        } else {
            $produtos = Produto::with(['fabricante', 'fornecedor'])->get();
        }
        return view('produto.index', compact('produtos','nome' ));
    }

    public function create()
    {
        $fabricantes = Fabricante::pluck( 'razao_social', 'id');
        $fornecedores= Fornecedor::pluck( 'razao_social', 'id');
        return view('produto.create', compact('fabricantes','fornecedores'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|max:255',
            'codigo' => 'required|max:255',
            'valor_compra' => 'required',
            'valor_venda' => 'required',
            'fabricante_id' => 'required|exists:fabricantes,id',
            'fornecedors_id' => 'required|exists:fornecedors,id'
        ]);

        Produto::create($request->all());
        return redirect()->route('produto.create')
            ->with('success','Produto registrado com sucesso!');
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'razao_social' => 'required|max:255'
        ]);

        //Fornecedor::create($request->all());
        return redirect()->route('produto.create')
            ->with('success','Fornecedor registrado com sucesso!');
    }

    public function destroy($id)
    {
        $produto = Produto::findOrFail($id);
        $produto->delete();
        return redirect()->route('produto.index')
            ->with('success','Produto removido com sucesso!');
    }
}
