<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{
    protected $fillable = [
        'razao_social', 'marca','fabricante_id','endereco','telefone'
    ];
}
